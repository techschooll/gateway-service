const BaseAppLoader = require('base-lib').BaseAppLoader
const Config = require('./config/service-config')
const Dependencies = {}
const h2o2 = require('h2o2')
const _ = require('lodash')
const uuid = require('uuid')


class App extends BaseAppLoader {
  constructor(config, dependencies) {
    dependencies.h2o2 = h2o2
    super(config, dependencies)
    super.startServer()
  }

  async registerRoutes(config) {
    this.server.route({
      method: '*',
      path: '/' + 'v1' + '/{restOfThePath*}',
      handler: {
        proxy: {
          mapUri: function(request) {
            let url = request.path
            let requestContext = {
              request_id : uuid.v4()
            }
            request.headers["x-request-context"] = JSON.stringify(requestContext)
            let redirectService = _.first(url.match(/\/v1\/[aA-zZ]*\//))
            if(config.proxy[redirectService]) {
              let redirectServiceConfig = config.proxy[redirectService]
              url = redirectServiceConfig.protocol +"://"+ redirectServiceConfig.host+ "/" + url.substring(redirectService.length)
            }
            return {
              uri: encodeURI(url)
            }
          },
          passThrough: true,
          xforward: true
        }
      }
    });
  }
}

var app = new App(Config, Dependencies)