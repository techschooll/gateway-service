let config = {
  "server": {
    "port": 3200,
    "host": "localhost"
  },
  "service": "gateway-service",
  "proxy": {
    "/v1/user/": {
      "host": "localhost:3400/v1/user",
      "protocol": "http"
    },
    "/v1/course/": {
      "host": "localhost:3600/v1/course",
      "protocol": "http"
    }
  }
}

module.exports = config